// if the database is empty on server start, create some sample data.
Meteor.startup(function () {
  if (Lists.find().count() === 0) {
    var data = [
      {name: "Book",
       contents: [
         ["Feeding People is Easy - www.paripublishing.com/en/books/feedingpeopleiseasy/book", "Environment", "Food"],
         ["More Like People - www.morelikepeople.org/", "Business", "Anarchism"],
       ]
      },
      {name: "Film",
       contents: [
         ["The Greatest Movie Ever Sold - bayproxy.me/torrent/6595063/The_Greatest_Movie_Ever_Sold_2011_LIMITED_DOCU_BDRip_XviD-PSYCHD", "Advertising", "Media", "Business"],
         ["Food, Inc - bayproxy.me/torrent/7046438/Food_Inc_(2008)_720p_BrRip_x264_-_550MB_-_YIFY", "Food", "Veg*nism", "Environment"],
         ["Samsara - bayproxy.me/torrent/7954458/Samsara_2011_BRRip_XviD_AC3-HDSi", "Environment"]
         ]
      },
      {name: "Video",
       contents: [
         ["Money and Guilt - www.youtube.com/watch?v=c7tlaF3dv_M", "Money"],
       ]
      },
      {name: "Article",
       contents: [
         ["American Psychosis - www.adbusters.org/magazine/90/hedges-american-psychosis.html", "Consumerism"],
       ]
      },
      {name: "Website",
       contents: [
         ["RiseUp - www.riseup.net", "Activism"],
       ]
      }
    ];

    var timestamp = (new Date()).getTime();
    for (var i = 0; i < data.length; i++) {
      var list_id = Lists.insert({name: data[i].name});
      for (var j = 0; j < data[i].contents.length; j++) {
        var info = data[i].contents[j];
        Todos.insert({list_id: list_id,
                      text: info[0],
                      timestamp: timestamp,
                      tags: info.slice(1)});
        timestamp += 1; // ensure unique timestamp.
      }
    }
  }
});
